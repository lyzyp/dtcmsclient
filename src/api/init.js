import { axios } from '@/utils/request'

const init = {

    // 添加系统初始化数据
    saveOrUpdate(parameter,flag) {
        if(flag === 0){
            return axios({
                url: '/api/v1/config/save',
                method: 'post',
                data: parameter
            })
        }else {
            return axios({
                url: '/api/v1/config/update',
                method: 'put',
                data: parameter
            })
        }
    },
    // 删除配置项
    remove(id) {
        return axios({
            url: '/api/v1/config/delete/'+id,
            method: 'delete'
        })
    },
    // 获取系统初始化参数类型数据
    getInitCategoryData() {
        return axios({
            url: '/api/v1/config/listCategory',
            method: 'get'
        })
    },
    // 获取系统初始化数据
    getInitData() {
        return axios({
            url: '/api/v1/config/init',
            method: 'get'
        })
    },
    // 获取系统主题列表
    queryThemeData() {
        return axios({
            url: '/api/v1/config/queryThemeData',
            method: 'get'
        })
    },
    // 获取系统参数配置列表
    list(parameter) {
        return axios({
            url: '/api/v1/config/list',
            method: 'get',
            params: parameter
        })
    },
    // 启用系统主题
    updateThemeEnabled(id) {
        return axios({
            url: '/api/v1/config/updateThemeEnabled/'+id,
            method: 'post'
        })
    },
    // 上传文件
    uploadFile(parameter) {
        return axios({
            url: '/api/v1/file/uploadCover',
            method: 'post',
            data: parameter
        })
    },
}

export default init;
