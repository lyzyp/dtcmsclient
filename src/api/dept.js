import { axios } from '@/utils/request'

const dept = {

    // 添加部门
    saveOrUpdate(parameter,flag) {
        if(flag === 0){
            return axios({
                url: '/api/v1/dept/save',
                method: 'post',
                data: parameter
            })
        }else {
            return axios({
                url: '/api/v1/dept/update',
                method: 'put',
                data: parameter
            })
        }
    },
    // 获取部门列表
    list(parameter) {
        return axios({
            url: '/api/v1/dept/list',
            method: 'get',
            params: parameter
        })
    },
    // 获取部门信息
    getById(id) {
        return axios({
            url: '/api/v1/dept/getById/'+id,
            method: 'get'
        })
    },
    // 删除部门
    remove(id) {
        return axios({
            url: '/api/v1/dept/delete/'+id,
            method: 'delete'
        })
    },
    // 获取部门树
    listTree(type) {
        return axios({
            url: '/api/v1/dept/listTree/'+type,
            method: 'get'
        })
    }
}

export default dept;
