import { axios } from '@/utils/request'

const menu = {

    // 添加菜单
    saveOrUpdate(parameter,flag) {
        if(flag === 0){
            return axios({
                url: '/api/v1/permission/save',
                method: 'post',
                data: parameter
            })
        }else {
            return axios({
                url: '/api/v1/permission/update',
                method: 'put',
                data: parameter
            })
        }
    },
    // 获取菜单列表
    list(parameter) {
        return axios({
            url: '/api/v1/permission/list',
            method: 'get',
            params: parameter
        })
    },
    // 获取菜单树
    listTree() {
        return axios({
            url: '/api/v1/permission/listTree',
            method: 'get'
        })
    },
    // 获取菜单信息
    getById(id) {
        return axios({
            url: '/api/v1/permission/getById/'+id,
            method: 'get'
        })
    },
    // 删除菜单
    remove(id) {
        return axios({
            url: '/api/v1/permission/delete/'+id,
            method: 'delete'
        })
    },
    // 添加角色菜单绑定
    saveRoleMenu(parameter) {
        return axios({
            url: '/api/v1/role-permission/saveRoleMenu',
            method: 'post',
            data: parameter
        })
    }
}

export default menu;
