export default function hasPermission(perms) {
    let hasTag = false;
    // 获取用户的按钮权限
    let permissions = JSON.parse(localStorage.getItem("authList"));
    for(let i = 0; i < permissions.length; i++) {
        if(permissions[i] === perms) {
            hasTag = true;
            break;
        }
    }
    return hasTag;
}
